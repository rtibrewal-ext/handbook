---
title: "Incubation Engineering Department Career Framework: Intermediate"
---

## Incubation Engineering Department Competencies: Intermediate

{{% include "includes/engineering/career-matrix-nav.md" %}}

**Intermediates at GitLab are expected to exhibit the following competencies:**

- [Intermediate Leadership Competencies](#intermediate-leadership-competencies)
- [Intermediate Technical Competencies](#intermediate-technical-competencies)
- [Intermediate Values Alignment](#intermediate-values-alignment)

---

### Intermediate Leadership Competencies

{{% include "includes/engineering/associate-leadership-competency.md" %}}
  
### Intermediate Technical Competencies

{{% include "includes/engineering/associate-technical-competency.md" %}}

### Intermediate Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
